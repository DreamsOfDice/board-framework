﻿using System.Collections;
using System.Collections.Generic;
using BoardGameFramework.ClassicBoards;
using BoardGameFramework.Core;
using BoardGameFramework.GenericImplementations;
using BoardGameFramework.HexBoards;
using BoardGameFramework.Pathfinding;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.TestTools;

namespace BoardGameFramework.PlaymodeTests
{
    public class BoardPathfindingTests
    {
        bool upkeepDone = false;
        GameObject testBoardObject;
        BoardPathGenerator generatorRef;
        [SetUp]
        public void Setup() {
            generatorRef = new BoardPathGenerator();
        }

        // A UnityTest behaves like a coroutine in Play Mode. In Edit Mode you can use
        // `yield return null;` to skip a frame.
        [UnityTest]
        public IEnumerator GeneratClassicPathTest()
        {
            SceneManager.LoadScene("ClassicBoardPlaymodeTestScene");
            yield return new WaitForSeconds(2f);

            testBoardObject = GameObject.Find("TestBoardObject");
            ClassicBoard testBoard = testBoardObject.GetComponent<ClassicBoard>();
            ClassicBoardData newData = new ClassicBoardData(4, 2);
            testBoard.SetData(newData);
            testBoard.SetupBoard();

            Assert.AreEqual(3,generatorRef.GeneratePath(testBoard.GetData(),testBoard.GetData().boardSpaces[0,0], testBoard.GetData().boardSpaces[2, 1],10).Count);
            yield return null;
        }

        [UnityTest]
        public IEnumerator GeneratHexPathTest()
        {
            SceneManager.LoadScene("HexBoardPlaymodeTestScene");
            yield return new WaitForSeconds(2f);

            testBoardObject = GameObject.Find("TestBoardObject");
            OffsetHexBoard testBoard = testBoardObject.GetComponent<OffsetHexBoard>();
            OffsetHexBoardData newData = new OffsetHexBoardData(3, 5, HexTypeEnum.ODD_Q_VERTICAL_LAYOUT);
            testBoard.SetData(newData);
            testBoard.SetupBoard();
            List<BoardSpaceBase> path = generatorRef.GeneratePath(testBoard.GetData(), testBoard.GetData().boardSpaces[0, 0], testBoard.GetData().boardSpaces[1, 4], 10);
            foreach (BoardSpaceBase b in path) {
                Debug.Log(b.name);

            }
            Assert.AreEqual(4, path.Count);
            yield return null;
        }

        [UnityTest]
        public IEnumerator GenerateClassicBoardPathTest()
        {
            SceneManager.LoadScene("ClassicBoardPlaymodeTestScene");
            yield return new WaitForSeconds(2f);

            testBoardObject = GameObject.Find("TestBoardObject");
            ClassicBoard testBoard = testBoardObject.GetComponent<ClassicBoard>();
            ClassicBoardData newData = new ClassicBoardData(4, 4);
            BoardPathGenerator boardMovement = new BoardPathGenerator();
            GenericBoardPiece boardpiece = GameObject.Find("PieceExample").GetComponent<GenericBoardPiece>();

            testBoard.SetData(newData);
            testBoard.SetupBoard();
            testBoard.GetData().boardSpaces[0,0].OccupySpace(boardpiece);
            testBoard.GetData().boardSpaces[1,0].GetData<ClassicBoardSpaceData>().Traversable = false;
            testBoard.GetData().boardSpaces[0,3].GetData<ClassicBoardSpaceData>().Traversable = false;
            testBoard.GetData().boardSpaces[1,1].GetData<ClassicBoardSpaceData>().Traversable = false;
            testBoard.GetData().boardSpaces[2,2].GetData<ClassicBoardSpaceData>().Traversable = false;

            Assert.AreEqual(5, boardMovement.GeneratePath(testBoard.GetData(),testBoard.GetData().boardSpaces[0,0],testBoard.GetData().boardSpaces[2,3],20).Count);
            
            Assert.AreEqual(true, boardMovement.GeneratePath(testBoard.GetData(),testBoard.GetData().boardSpaces[0,0],testBoard.GetData().boardSpaces[2,3],20).Contains(testBoard.GetData().boardSpaces[0, 1]));
            Assert.AreEqual(true, boardMovement.GeneratePath(testBoard.GetData(),testBoard.GetData().boardSpaces[0,0],testBoard.GetData().boardSpaces[2,3],20).Contains(testBoard.GetData().boardSpaces[0, 2]));
            Assert.AreEqual(true, boardMovement.GeneratePath(testBoard.GetData(),testBoard.GetData().boardSpaces[0,0],testBoard.GetData().boardSpaces[2,3],20).Contains(testBoard.GetData().boardSpaces[1, 2]));
            Assert.AreEqual(true, boardMovement.GeneratePath(testBoard.GetData(),testBoard.GetData().boardSpaces[0,0],testBoard.GetData().boardSpaces[2,3],20).Contains(testBoard.GetData().boardSpaces[1, 3]));
            Assert.AreEqual(true, boardMovement.GeneratePath(testBoard.GetData(),testBoard.GetData().boardSpaces[0,0],testBoard.GetData().boardSpaces[2,3],20).Contains(testBoard.GetData().boardSpaces[2, 3]));

            yield return null;
        }

    }
}
