﻿using System.Collections;
using System.Collections.Generic;
using BoardGameFramework.HexBoards;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.TestTools;

namespace BoardGameFramework.PlaymodeTests
{
    public class HexBoardPlaymodeTests
    {
        bool upkeepDone = false;
        GameObject testBoardObject;
        [SetUp]
        public void Setup()
        {
            SceneManager.LoadScene("HexBoardPlaymodeTestScene");
            SceneManager.sceneLoaded += Upkeep;

        }

        [TearDown]
        public void CleanUp()
        {
            SceneManager.sceneLoaded -= Upkeep;
            upkeepDone = false;

        }

        public void Upkeep(Scene scene, LoadSceneMode mode)
        {
            if (scene.name != "HexBoardPlaymodeTestScene")
            {
                return;
            }
            Debug.Log("I loaded");
            testBoardObject = GameObject.Find("TestBoardObject");
            upkeepDone = true;

        }

       
        // A UnityTest behaves like a coroutine in Play Mode. In Edit Mode you can use
        // `yield return null;` to skip a frame.
        [UnityTest]
        public IEnumerator HexBoardGenerationTest()
        {
            OffsetHexBoard testBoard = testBoardObject.GetComponent<OffsetHexBoard>();
            OffsetHexBoardData newData = new OffsetHexBoardData(4, 2,HexTypeEnum.ODD_Q_VERTICAL_LAYOUT);
            testBoard.SetData(newData);
            testBoard.SetupBoard();
            yield return new WaitForSeconds(2f);
            Assert.IsNotNull(testBoard.GetData().boardSpaces[2, 1]);
            Assert.AreEqual(8, testBoard.GetData().boardSpaces.Length);
            Assert.AreEqual(8, testBoard.gameObject.transform.GetChild(0).transform.childCount);

            Assert.AreNotEqual(testBoard.gameObject.transform.GetChild(0).gameObject.transform.GetChild(0).transform.position.y, testBoard.gameObject.transform.GetChild(0).gameObject.transform.GetChild(1).transform.position.y);
            //Assert.AreEqual(testBoard.gameObject.transform.GetChild(0).gameObject.transform.GetChild(0).transform.position.y+ testBoard.GetComponent<OffsetHexBoard>().hexBoardSpace.GetComponent<SpriteRenderer>().size.y, testBoard.gameObject.transform.GetChild(0).gameObject.transform.GetChild(1).transform.position.y);

            Assert.AreEqual(2, testBoard.GetData().boardSpaces[2, 1].GetData<OffsetHexBoardSpaceData>().Row);
            Assert.AreEqual(1, testBoard.GetData().boardSpaces[2, 1].GetData<OffsetHexBoardSpaceData>().Column);
            yield return null;
        }

        [UnityTest]
        public IEnumerator HexBoardTraversableSpacesTest()
        {
            OffsetHexBoard testBoard = testBoardObject.GetComponent<OffsetHexBoard>();
            OffsetHexBoardData newData = new OffsetHexBoardData(4, 2, HexTypeEnum.ODD_Q_VERTICAL_LAYOUT);
            testBoard.SetData(newData);
            testBoard.SetupBoard();
            testBoard.GetData().boardSpaces[1, 1].GetData<OffsetHexBoardSpaceData>().Traversable = false;
            Assert.AreEqual(true, testBoard.GetData().boardSpaces[0, 1].GetData<OffsetHexBoardSpaceData>().Traversable);
            Assert.AreEqual(false, testBoard.GetData().boardSpaces[1, 1].GetData<OffsetHexBoardSpaceData>().Traversable);
            Assert.AreEqual(true, testBoard.GetData().boardSpaces[3, 0].GetData<OffsetHexBoardSpaceData>().Traversable);


            yield return null;
        }

        [UnityTest]
        public IEnumerator HexBoardNeighbour6wayGeneration()
        {
            OffsetHexBoard testBoard = testBoardObject.GetComponent<OffsetHexBoard>();
            OffsetHexBoardData newData = new OffsetHexBoardData(8, 8, HexTypeEnum.ODD_Q_VERTICAL_LAYOUT);
            testBoard.SetData(newData);
            testBoard.SetupBoard();

            Assert.AreEqual(6, testBoard.GetData().boardSpaces[3, 2].GetData<OffsetHexBoardSpaceData>().Neighbours.Count);
            Assert.AreEqual(2, testBoard.GetData().boardSpaces[0, 0].GetData<OffsetHexBoardSpaceData>().Neighbours.Count);
            Assert.AreEqual(4, testBoard.GetData().boardSpaces[4, 0].GetData<OffsetHexBoardSpaceData>().Neighbours.Count);
            Assert.AreEqual(4, testBoard.GetData().boardSpaces[3, 0].GetData<OffsetHexBoardSpaceData>().Neighbours.Count);

           

            Assert.AreEqual(true, testBoard.GetData().boardSpaces[3, 2].GetData<OffsetHexBoardSpaceData>().Neighbours.Contains(testBoard.GetData().boardSpaces[2, 2]));
            Assert.AreEqual(true, testBoard.GetData().boardSpaces[3, 2].GetData<OffsetHexBoardSpaceData>().Neighbours.Contains(testBoard.GetData().boardSpaces[2, 3]));
            Assert.AreEqual(true, testBoard.GetData().boardSpaces[3, 2].GetData<OffsetHexBoardSpaceData>().Neighbours.Contains(testBoard.GetData().boardSpaces[3, 1]));
            Assert.AreEqual(true, testBoard.GetData().boardSpaces[3, 2].GetData<OffsetHexBoardSpaceData>().Neighbours.Contains(testBoard.GetData().boardSpaces[4, 2]));
            Assert.AreEqual(true, testBoard.GetData().boardSpaces[3, 2].GetData<OffsetHexBoardSpaceData>().Neighbours.Contains(testBoard.GetData().boardSpaces[2, 1]));
            Assert.AreEqual(true, testBoard.GetData().boardSpaces[3, 2].GetData<OffsetHexBoardSpaceData>().Neighbours.Contains(testBoard.GetData().boardSpaces[3, 3]));
                        
            yield return null;
        }

        [UnityTest]
        public IEnumerator HexBoardOccupySpaceTest()
        {
            OffsetHexBoard testBoard = testBoardObject.GetComponent<OffsetHexBoard>();
            OffsetHexBoardData newData = new OffsetHexBoardData(4, 2, HexTypeEnum.ODD_Q_VERTICAL_LAYOUT);
            testBoard.SetData(newData);
            testBoard.SetupBoard();
            GameObject testObject = GameObject.Find("PieceExample");
            HexBoardPiece testPiece = testObject.AddComponent<HexBoardPiece>();
            testPiece.data = new HexBoardPieceData();
            Assert.True(testBoard.GetData().boardSpaces[1, 1].OccupySpace(testPiece));
            Assert.AreEqual(1, testBoard.GetData().boardSpaces[1, 1].GetData<OffsetHexBoardSpaceData>().Occupants.Count);
            Assert.AreEqual(0, testBoard.GetData().boardSpaces[2, 0].GetData<OffsetHexBoardSpaceData>().Occupants.Count);

            yield return null;
        }

    }
}
