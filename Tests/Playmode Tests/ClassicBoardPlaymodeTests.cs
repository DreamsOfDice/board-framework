﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using BoardGameFramework.ClassicBoards;
using BoardGameFramework.Core;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.TestTools;

namespace BoardGameFramework.PlaymodeTests
{
    public class ClassicBoardPlaymodeTests
    {
        bool upkeepDone = false;
        GameObject testBoardObject;
        [SetUp]
        public void Setup() {
            SceneManager.LoadScene("ClassicBoardPlaymodeTestScene");
            SceneManager.sceneLoaded += Upkeep;

        }

        [TearDown]
        public void CleanUp()
        {
            SceneManager.sceneLoaded -= Upkeep;
            upkeepDone = false;

        }
        
        // A UnityTest behaves like a coroutine in Play Mode. In Edit Mode you can use
        // `yield return null;` to skip a frame.
        [UnityTest]
        public IEnumerator ClassicBoardGenerationTest()
        {
            ClassicBoard testBoard = testBoardObject.GetComponent<ClassicBoard>();
            ClassicBoardData newData = new ClassicBoardData(4,2);
            testBoard.SetData(newData);
            testBoard.SetupBoard();
            Assert.IsNotNull(testBoard.GetData().boardSpaces[2,1]);
            Assert.AreEqual(8 ,testBoard.GetData().boardSpaces.Length);
            Assert.AreEqual(8, testBoard.gameObject.transform.GetChild(0).transform.childCount);
            Assert.AreEqual(2, testBoard.GetData().boardSpaces[2, 1].GetData<ClassicBoardSpaceData>().Row);
            Assert.AreEqual(1, testBoard.GetData().boardSpaces[2, 1].GetData<ClassicBoardSpaceData>().Column);

            yield return null;
        }

        [UnityTest]
        public IEnumerator ClassicBoardTraversableSpacesTest()
        {
            ClassicBoard testBoard = testBoardObject.GetComponent<ClassicBoard>();
            ClassicBoardData newData = new ClassicBoardData(4, 2);
            testBoard.SetData(newData);
            testBoard.SetupBoard();
            testBoard.GetData().boardSpaces[1, 1].GetData<ClassicBoardSpaceData>().Traversable = false;
            Assert.AreEqual(true, testBoard.GetData().boardSpaces[0, 1].GetData<ClassicBoardSpaceData>().Traversable);
            Assert.AreEqual(false,testBoard.GetData().boardSpaces[1, 1].GetData<ClassicBoardSpaceData>().Traversable);
            Assert.AreEqual(true, testBoard.GetData().boardSpaces[3, 0].GetData<ClassicBoardSpaceData>().Traversable);


            yield return null;
        }

        [UnityTest]
        public IEnumerator ClassicNeighbour4wayGeneration()
        {
            ClassicBoard testBoard = testBoardObject.GetComponent<ClassicBoard>();
            ClassicBoardData newData = new ClassicBoardData(4, 3);
            testBoard.SetData(newData);
            testBoard.SetupBoard();
            Assert.AreEqual(4, testBoard.GetData().boardSpaces[1, 1].GetData<ClassicBoardSpaceData>().Neighbours.Count);
            Assert.AreEqual(2, testBoard.GetData().boardSpaces[0, 0].GetData<ClassicBoardSpaceData>().Neighbours.Count);

            Assert.AreEqual(false, testBoard.GetData().boardSpaces[1, 1].GetData<ClassicBoardSpaceData>().Neighbours.Contains(testBoard.GetData().boardSpaces[3, 0]));
            Assert.AreEqual(false, testBoard.GetData().boardSpaces[1, 1].GetData<ClassicBoardSpaceData>().Neighbours.Contains(testBoard.GetData().boardSpaces[3, 2]));

            Assert.AreEqual(true, testBoard.GetData().boardSpaces[1, 1].GetData<ClassicBoardSpaceData>().Neighbours.Contains(testBoard.GetData().boardSpaces[1, 0]));
            Assert.AreEqual(true, testBoard.GetData().boardSpaces[1, 1].GetData<ClassicBoardSpaceData>().Neighbours.Contains(testBoard.GetData().boardSpaces[1, 2]));
            Assert.AreEqual(true, testBoard.GetData().boardSpaces[1, 1].GetData<ClassicBoardSpaceData>().Neighbours.Contains(testBoard.GetData().boardSpaces[0, 1]));
            Assert.AreEqual(true, testBoard.GetData().boardSpaces[1, 1].GetData<ClassicBoardSpaceData>().Neighbours.Contains(testBoard.GetData().boardSpaces[2, 1]));


            yield return null;
        }

        [UnityTest]
        public IEnumerator ClassicNeighbour8wayGeneration()
        {
            ClassicBoard testBoard = testBoardObject.GetComponent<ClassicBoard>();
            ClassicBoardData newData = new ClassicBoardData(6,6,Enums.BoardNeighborType.EIGHT_WAY_SQUARE_NEIGHBOR);
            testBoard.SetData(newData);
            testBoard.SetupBoard();
            Assert.AreEqual(8, testBoard.GetData().boardSpaces[1, 1].GetData<ClassicBoardSpaceData>().Neighbours.Count);
            Assert.AreEqual(3, testBoard.GetData().boardSpaces[0, 0].GetData<ClassicBoardSpaceData>().Neighbours.Count);

            Assert.AreEqual(false, testBoard.GetData().boardSpaces[1, 1].GetData<ClassicBoardSpaceData>().Neighbours.Contains(testBoard.GetData().boardSpaces[5, 5]));
            Assert.AreEqual(false, testBoard.GetData().boardSpaces[1, 1].GetData<ClassicBoardSpaceData>().Neighbours.Contains(testBoard.GetData().boardSpaces[4, 2]));

            Assert.AreEqual(true, testBoard.GetData().boardSpaces[1, 1].GetData<ClassicBoardSpaceData>().Neighbours.Contains(testBoard.GetData().boardSpaces[1, 0]));
            Assert.AreEqual(true, testBoard.GetData().boardSpaces[1, 1].GetData<ClassicBoardSpaceData>().Neighbours.Contains(testBoard.GetData().boardSpaces[1, 2]));
            Assert.AreEqual(true, testBoard.GetData().boardSpaces[1, 1].GetData<ClassicBoardSpaceData>().Neighbours.Contains(testBoard.GetData().boardSpaces[0, 1]));
            Assert.AreEqual(true, testBoard.GetData().boardSpaces[1, 1].GetData<ClassicBoardSpaceData>().Neighbours.Contains(testBoard.GetData().boardSpaces[2, 1]));

            Assert.AreEqual(true, testBoard.GetData().boardSpaces[1, 1].GetData<ClassicBoardSpaceData>().Neighbours.Contains(testBoard.GetData().boardSpaces[0, 0]));
            Assert.AreEqual(true, testBoard.GetData().boardSpaces[1, 1].GetData<ClassicBoardSpaceData>().Neighbours.Contains(testBoard.GetData().boardSpaces[0, 2]));
            Assert.AreEqual(true, testBoard.GetData().boardSpaces[1, 1].GetData<ClassicBoardSpaceData>().Neighbours.Contains(testBoard.GetData().boardSpaces[2, 0]));
            Assert.AreEqual(true, testBoard.GetData().boardSpaces[1, 1].GetData<ClassicBoardSpaceData>().Neighbours.Contains(testBoard.GetData().boardSpaces[2, 2]));

            yield return null;
        }


        [UnityTest]
        public IEnumerator ClassicBoardOccupySpaceTest()
        {
            ClassicBoard testBoard = testBoardObject.GetComponent<ClassicBoard>();
            ClassicBoardData newData = new ClassicBoardData(4, 3);
            testBoard.SetData(newData);
            testBoard.SetupBoard();
            GameObject testObject = GameObject.Find("PieceExample");
            ClassicBoardPiece testPiece = testObject.AddComponent<ClassicBoardPiece>();
            //make classic piece
            Assert.True(testBoard.GetData().boardSpaces[1, 1].OccupySpace(testPiece));
            Assert.AreEqual(1,testBoard.GetData().boardSpaces[1, 1].GetData<ClassicBoardSpaceData>().Occupants.Count);
            Assert.AreEqual(0, testBoard.GetData().boardSpaces[2, 0].GetData<ClassicBoardSpaceData>().Occupants.Count);

            yield return null;
        }


        public void Upkeep(Scene scene, LoadSceneMode mode)
        {
            if (scene.name != "ClassicBoardPlaymodeTestScene")
            {
                return;
            }
            Debug.Log("I loaded");
            testBoardObject = GameObject.Find("TestBoardObject");
             upkeepDone = true;

        }


    }
}
