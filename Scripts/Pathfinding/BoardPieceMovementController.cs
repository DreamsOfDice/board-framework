﻿using BoardGameFramework.Core;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BoardGameFramework.Pathfinding
{
    public class BoardPieceMovementController 
    {
        //List<BoardPieceBase> piecesToMove = new List<BoardPieceBase>();
        protected List<BoardSpaceBase> currentPath = new List<BoardSpaceBase>();
        // How far this unit can move in one turn. Note that some tiles cost extra.
        protected float speed = 2;   

          public bool IsPathEmpty()
        {
            if (currentPath.Count == 0)
            {
                return true;
            }
            else {
                return false;
            }
        }

        public virtual bool MovementHandling(BoardPieceBase pieceToMove)
        {
            // Have we moved our visible piece close enough to the target tile that we can
            // advance to the next step in our pathfinding?
            // Move our position a step closer to the target.
            if (IsPathEmpty()) {
                return false; //you should not be calling me, as im empty, you should call ispathempty before calling me and should know this
            }

            float step = speed * Time.deltaTime; // calculate distance to move
            pieceToMove.transform.position = Vector3.MoveTowards(pieceToMove.transform.position, currentPath[0].transform.position, step);

            // Check if the position of the cube and sphere are approximately equal.
            if (Vector3.Distance(pieceToMove.transform.position, currentPath[0].transform.position) < 0.001f)
            {
                int indexOfPlayer = 0;
                bool foundPlayer = false;
                // hard code the position.
                BoardSpaceBase sp = pieceToMove.GetData<BoardPieceData>().currentSpace;
                int amount = sp.GetData<BoardSpaceData>().Occupants.Count;
                for (int i = 0; i< amount; i++ ) {
                    if (pieceToMove.GetData<BoardPieceData>().currentSpace.GetData<BoardSpaceData>().Occupants[i] == pieceToMove) {
                         indexOfPlayer = i;
                        foundPlayer = true;
                        break;
                    }
                }
                if (foundPlayer) {
                    pieceToMove.GetData<BoardPieceData>().currentSpace.RemoveCharacterFromCurrentSpace(pieceToMove); //GetData<BoardSpaceData>().Occupants.RemoveAt(indexOfPlayer);

                }
                pieceToMove.GetData<BoardPieceData>().currentSpace = currentPath[0];
                currentPath[0].OccupySpace(pieceToMove);
                AdvancePathing();
            }
                        
            return true;
        }

        public virtual void SetNewPath(List<BoardSpaceBase> newPath,float newSpeed)
        {
            currentPath = newPath;
            speed = newSpeed;
        }

        protected virtual void AdvancePathing()
        {
            if (currentPath == null)
                return;
                        
            currentPath.RemoveAt(0);

          
        }


    }
}