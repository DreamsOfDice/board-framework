﻿using BoardGameFramework.Core;
using System.Collections.Generic;
using System.Linq;
namespace BoardGameFramework.Pathfinding {
    public class BoardPathRangeHandler {
        protected BoardData boardInfo;

        public BoardPathRangeHandler(BoardData boardToHandle) {
            boardInfo = boardToHandle;
        }

        public List<BoardSpaceBase> GetRawRange(int rangeValue, bool mustBeAPartOfADirectPath, BoardSpaceData spaceToCheckRangeFrom) {
            HashSet<BoardSpaceBase> spacesInRangeToReturn = new HashSet<BoardSpaceBase>();
            for (int i = 0; i < rangeValue; i++) {
                for (int j = 0; j < rangeValue; j++) {
                    spacesInRangeToReturn = AddSpacesToHashSet(GetNeighbours(boardInfo.boardSpaces[spaceToCheckRangeFrom.Column + i, spaceToCheckRangeFrom.Row + i], true),spacesInRangeToReturn);
                }
            }
            return spacesInRangeToReturn.ToList();
        }

        public List<BoardSpaceBase> GetPathableSpaces(int rangeValue, bool mustBeAPartOfADirectPath, BoardSpaceData spaceToCheckRangeFrom) {

            HashSet<BoardSpaceBase> spacesInRangeToReturn = new HashSet<BoardSpaceBase>();
            for (int i = 0; i < rangeValue; i++) {
                for (int j = 0; j < rangeValue; j++) {
                    spacesInRangeToReturn = AddSpacesToHashSet(GetNeighbours(boardInfo.boardSpaces[spaceToCheckRangeFrom.Column + i, spaceToCheckRangeFrom.Row + i],false),spacesInRangeToReturn);
                    if (i == 0 && j == 0 && mustBeAPartOfADirectPath && spacesInRangeToReturn.Count == 0) {
                        return spacesInRangeToReturn.ToList();
                    }
                }
            }
            return spacesInRangeToReturn.ToList();
        }

        protected virtual HashSet<BoardSpaceBase> GetNeighbours(BoardSpaceBase spaceToCheck,bool canBeOccupied) {
            HashSet<BoardSpaceBase> occupiableNeighbours = new HashSet<BoardSpaceBase>();
            foreach (BoardSpaceBase n in spaceToCheck.GetData<BoardSpaceData>().Neighbours) {
                if (n.GetData<BoardSpaceData>().Occupiable || canBeOccupied) {
                    occupiableNeighbours.Add(n);
                }
            }
            return occupiableNeighbours;
        }

        protected HashSet<BoardSpaceBase> AddSpacesToHashSet(HashSet<BoardSpaceBase> setToAddFrom, HashSet<BoardSpaceBase> setToAddTo) {
            foreach (BoardSpaceBase space in setToAddFrom) {
                setToAddTo.Add(space);
            }
            return setToAddTo;
        }

    }
}