﻿using BoardGameFramework.Core;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace BoardGameFramework.Pathfinding
{
    public class BoardPathGenerator 
    {

        public virtual List<BoardSpaceBase> GeneratePath(BoardData boardToUSe, BoardSpaceBase startSpace, BoardSpaceBase endSpace, int amountOfActions)
        {


            List<BoardSpaceBase> pathToReturn = new List<BoardSpaceBase>();
            //List<BoardSpaceBase> boardSpaces = new List<BoardSpaceBase>(boardToUSe.GetData().boardSpaces);


            Dictionary<BoardSpaceBase, float> dist = new Dictionary<BoardSpaceBase, float>();
            Dictionary<BoardSpaceBase, BoardSpaceBase> prev = new Dictionary<BoardSpaceBase, BoardSpaceBase>();

            // Setup the queue aka the list of nodes we haven't checked yet.
            List<BoardSpaceBase> unvisited = new List<BoardSpaceBase>();

            BoardSpaceBase source = startSpace;

            BoardSpaceBase target = endSpace;

            if (target.GetData<BoardSpaceData>().Occupiable == false)
            {
                // We probably clicked on a mountain or something, so just quit out.
                return null;
            }

            dist[source] = 0;
            prev[source] = null;

            // Initialize everything to have INFINITY distance, since
            // we don't know any better right now. Also, it's possible
            // that some nodes CAN'T be reached from the source,
            // which would make INFINITY a reasonable value
//            foreach (RowsHelperFunction row in boardSpaces){
  //              List<BoardSpace> spacesOfRow = row.GetColumns();
                foreach (BoardSpaceBase space in boardToUSe.boardSpaces)
                {
                    if (space != source)
                    {
                        dist[space] = Mathf.Infinity;
                        prev[space] = null;
                    }

                    unvisited.Add(space);
                }

           // }

            while (unvisited.Count > 0)
            {
                // "u" is going to be the unvisited node with the smallest distance.
                BoardSpaceBase nodeBeingTested = null;

                foreach (BoardSpaceBase possibleU in unvisited)
                {
                    if (nodeBeingTested == null || dist[possibleU] < dist[nodeBeingTested])//first time it always goes in
                    {
                        nodeBeingTested = possibleU;
                    }
                }

                if (nodeBeingTested == target)
                {
                    break;  // Exit the while loop!
                }

                unvisited.Remove(nodeBeingTested);

                foreach (BoardSpaceBase v in nodeBeingTested.GetData<BoardSpaceData>().Neighbours)
                {
                    //float alt = dist[u] + u.DistanceTo(v);
                    float alt = dist[nodeBeingTested] + CostToEnterTile(nodeBeingTested, v);
                    if (alt < dist[v])
                    {
                        dist[v] = alt;
                        prev[v] = nodeBeingTested;
                    }
                }
            }

            // If we get there, the either we found the shortest route
            // to our target, or there is no route at ALL to our target.

            if (prev[target] == null)
            {
                // No route between our target and the source
                return null;
            }


            BoardSpaceBase curr = target;

            // Step through the "prev" chain and add it to our path
            while (curr != null)
            {
                pathToReturn.Add(curr);
                curr = prev[curr];
            }

            // Right now, currentPath describes a route from out target to our source
            // So we need to invert it!

            if (pathToReturn.Count == 0)
            {//this check is to avoid the remove at 0 exception
                return pathToReturn;
            }
            pathToReturn.Reverse();
            pathToReturn.RemoveAt(0);//no need to have initial node in path

            if (pathToReturn.Sum(it => it.GetData<BoardSpaceData>().SpaceCost) > amountOfActions)
                return null;

            return pathToReturn;

        }

        public virtual float CostToEnterTile(BoardSpaceBase pointOfEntry, BoardSpaceBase pointToEnter)
        {

            if (pointToEnter.GetData<BoardSpaceData>().Traversable == false)//pointToEnter.GetIsOccupiable() == false ||
                return Mathf.Infinity;

            float cost = pointToEnter.GetData<BoardSpaceData>().SpaceCost;

            if (pointOfEntry.GetData<BoardSpaceData>().Row != pointToEnter.GetData<BoardSpaceData>().Row && pointOfEntry.GetData<BoardSpaceData>().Column != pointToEnter.GetData<BoardSpaceData>().Column)
            {
                // We are moving diagonally!  Fudge the cost for tie-breaking
                // Purely a cosmetic thing!
                cost += 0.001f;
            }

            return cost;

        }
    }
}