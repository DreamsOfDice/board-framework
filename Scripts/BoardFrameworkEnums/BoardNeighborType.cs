﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BoardGameFramework.Enums
{
    public enum BoardNeighborType
    {
        FOUR_WAY_SQUARE_NEIGHBOR, EIGHT_WAY_SQUARE_NEIGHBOR
    }
}