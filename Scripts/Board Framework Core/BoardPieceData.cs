﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BoardGameFramework.Core
{
    [Serializable]
    public class BoardPieceData 
    {
        public BoardSpaceBase currentSpace;
        public Vector3 currentGraphicalPosition;
        public float pieceMovementSpeed;

        public List<BoardSpaceBase> currentPath = new List<BoardSpaceBase>();

    }
}