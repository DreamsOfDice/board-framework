﻿using System;
using System.Collections;
using System.Collections.Generic;
using Unity.Collections;
using UnityEngine;

namespace BoardGameFramework.Core
{
    [Serializable]
    public class BoardSpaceData 
    {
        [SerializeField]
        bool traversable  = true;
       
        [SerializeField]
        bool canOccupy = true;

        [SerializeField]
        int amountOfPossibleOccupants = 1;

        [SerializeField]
        [ReadOnly]
        int rowInBoard;

        [SerializeField]
        [ReadOnly]
        int columnInBoard;

        [SerializeField]
        int spaceEntryCost = 1;

        [SerializeField]
        [ReadOnly]
        List<BoardSpaceBase> neighbourSpaces = new List<BoardSpaceBase>();

        [SerializeField]
        [ReadOnly]
        List<BoardPieceBase> occupantsPiecesList =new List<BoardPieceBase>();

        public BoardSpaceData(int row, int column) {
            rowInBoard = row;
            columnInBoard = column;
        }

        #region getters and setters
        public bool Traversable
        {
            get => traversable;
            set => traversable = value;
        }

        public bool Occupiable
        {
            get => canOccupy;
            set => canOccupy = value;
        }

        public int OccupantsAmount
        {
            get => amountOfPossibleOccupants;
            set => amountOfPossibleOccupants = value;
        }

        public int SpaceCost
        {
            get => spaceEntryCost;
            set => spaceEntryCost = value;
        }

        public List<BoardSpaceBase> Neighbours
        {
            get => neighbourSpaces;
            set => neighbourSpaces = value;
        }

        public List<BoardPieceBase> Occupants
        {
            get => occupantsPiecesList;
            set => occupantsPiecesList = value;
        }

        public int Row
        {
            get => rowInBoard;
        }

        public int Column
        {
            get => columnInBoard;
        }

        #endregion
    }
}