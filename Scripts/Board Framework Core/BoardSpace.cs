﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BoardGameFramework.Core
{
    public abstract class BoardSpace<T> : BoardSpaceBase where T: BoardSpaceData
    {
        [SerializeField]
        public T data;

        public override T2 GetData<T2>()
        {
            return data as T2;
        }

        public override bool OccupySpace(BoardPieceBase piece)
        {
            if (data.Occupants.Count >= data.OccupantsAmount) {
                return false;
            }
           // List<BoardPieceBase> newPiecesList = new List<BoardPieceBase>();
            //newPiecesList.Add(piece);
            data.Occupants.Add(piece);//used to add newpieceslist
            piece.SetPosition(gameObject.transform.position,this);           
            return true;
        }

        /// <summary>
        /// used to remove the piece from its current space
        /// </summary>
        public override bool RemoveCharacterFromCurrentSpace(BoardPieceBase pieceToRemove)
        {
            bool foundPlayer = false;
            int indexOfPlayer = 0;

            //remove yourself and keep advancing
            for (int i = 0; i < data.Occupants.Count; i++)
            {
                if (data.Occupants[i] == pieceToRemove)
                {
                    indexOfPlayer = i;
                    foundPlayer = true;
                    break;
                }
            }
            if (foundPlayer)
            {
                data.Occupants.RemoveAt(indexOfPlayer);
                return true;
            }
            return false;
        }

    }
}