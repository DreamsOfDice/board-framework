﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BoardGameFramework.Core
{
    public abstract class BoardPieceBase : MonoBehaviour
    {
       // public float minPosition = 0.001f;

        public abstract T GetData<T>() where T : BoardPieceData;
        public abstract void MovementHandling();
        public abstract void SetPosition(Vector3 newPosition, BoardSpaceBase newCurrentSpace);
    }
}