﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BoardGameFramework.Core
{
    public abstract class BoardSpaceBase : MonoBehaviour
    {
        public abstract T GetData<T>() where T : BoardSpaceData;
        public abstract bool OccupySpace(BoardPieceBase piece);
        public abstract bool RemoveCharacterFromCurrentSpace(BoardPieceBase piece);

    }
}