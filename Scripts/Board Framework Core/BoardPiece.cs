﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BoardGameFramework.Core
{
    public abstract class BoardPiece<T> : BoardPieceBase where T: BoardPieceData
    {
        public T data;
         
        public override T2 GetData<T2>()
        {
            return data as T2;
        }


        public override void SetPosition(Vector3 newPosition, BoardSpaceBase newCurrentSpace){
             gameObject.transform.position = newPosition;
             data.currentSpace = newCurrentSpace;       
         }


        public override void MovementHandling()
        {
            // Have we moved our visible piece close enough to the target tile that we can
            // advance to the next step in our pathfinding?
            // Move our position a step closer to the target.

            if (data.currentSpace != null) {
                data.currentSpace.RemoveCharacterFromCurrentSpace(this);
            }
            float step = data.pieceMovementSpeed * Time.deltaTime; // calculate distance to move
            transform.position = Vector3.MoveTowards(transform.position, data.currentPath[0].transform.position, step);
            if (data.currentPath[0].GetData<BoardSpaceData>().Occupants.Contains(this) == false) {
                data.currentPath[0].GetData<BoardSpaceData>().Occupants.Add(this);
            }
            // Check if the position of the cube and sphere are approximately equal.
            if (Vector3.Distance(transform.position, data.currentPath[0].transform.position) < 0.001f)
            {
                data.currentSpace = data.currentPath[0];
                data.currentSpace.RemoveCharacterFromCurrentSpace(this);//remove myself from the space since i added myself in the mean time to avoid others moving there
                data.currentPath[0].OccupySpace(this);//reset myself and get centered

                // hard code the position.

                if (data.currentPath.Count > 1)
                {
                    if (data.currentPath[1].GetData<BoardSpaceData>().OccupantsAmount == data.currentPath[1].GetData<BoardSpaceData>().Occupants.Count)
                    {
                        data.currentPath = new List<BoardSpaceBase>();
                        return;
                    }

                    data.currentSpace.RemoveCharacterFromCurrentSpace(this);


                }

                //pieceToMove.SetPositon(data.currentPath[0].GetBoardUIObject().transform.position, currentPath[0].GetBoardUIObject().spaceControler);

                if (data.currentPath.Count == 0)
                    return;

                data.currentPath.RemoveAt(0);
            }

        }


    }
}