﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BoardGameFramework.Core
{
    public abstract class Board<T>: MonoBehaviour where T : BoardData{ 

        [SerializeField]
        protected T data;

        [SerializeField]
        protected List<BoardRow> organizationalRows;
             

        public T GetData() {
            return data;
        }

        public virtual void SetData(T newData) {
            data = newData;
        }

        public abstract void SetupBoard();
       
        protected abstract void CalculateNeighbours();
    }
}
