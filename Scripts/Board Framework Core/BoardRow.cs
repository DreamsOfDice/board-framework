﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BoardGameFramework.Core
{
    [Serializable]
    public class BoardRow 
    {
        [SerializeField]
        public List<GameObject> uiSpaces;
    }
}