﻿using BoardGameFramework.Enums;
using System;
using System.Collections;
using System.Collections.Generic;
using Unity.Collections;
using UnityEngine;

namespace BoardGameFramework.Core
{
    [Serializable]
    public class BoardData
    {
        [SerializeField]
        [ReadOnly]
        public BoardSpaceBase[,] boardSpaces ;
        [SerializeField]
        public int columns;
        [SerializeField]
        public int rows;
              
        
        public BoardData(int newColumns, int newRows)
        {
            columns = newColumns;
            rows = newRows;
            boardSpaces = new BoardSpaceBase[columns, rows]; 
        }
        
       

    }
}