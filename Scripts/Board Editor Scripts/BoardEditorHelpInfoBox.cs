﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BoardGameFramework.CustomBoardEditor
{
    public class BoardEditorHelpInfoBox : PropertyAttribute
    {
        public string text;

        public BoardEditorHelpInfoBox(string newText)
        {
            text = newText;
        }
    }
}