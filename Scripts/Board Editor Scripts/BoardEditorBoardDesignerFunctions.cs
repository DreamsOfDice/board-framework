﻿using System.Collections;
using System.Collections.Generic;

#if UNITY_EDITOR
using UnityEditor;
using UnityEngine;

namespace BoardGameFramework.CustomBoardEditor
{
    [CustomEditor(typeof(BoardEditorBoardDesigner))]
    public class BoardEditorBoardDesignerFunctions : UnityEditor.Editor
    {
        public override void OnInspectorGUI()
        {

            base.OnInspectorGUI();

            BoardEditorBoardDesigner boardDesigner = (BoardEditorBoardDesigner)target;
            if (GUILayout.Button("Build Classic Board") && boardDesigner.rows != 0 && boardDesigner.columns != 0)
            {
                boardDesigner.GenerateClassicBoard();
            }

            if (GUILayout.Button("Build Offset Hexagonal Board") && boardDesigner.rows != 0 && boardDesigner.columns != 0)
            {
                boardDesigner.GenerateOffsetHexagonalBoard();
            }

        }
    }
}
#endif