﻿using BoardGameFramework.ClassicBoards;
using BoardGameFramework.Core;
using BoardGameFramework.HexBoards;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BoardGameFramework.CustomBoardEditor
{

    public class BoardEditorBoardDesigner : MonoBehaviour
    {
        [BoardEditorHelpInfoBox("You must ensure the values of row and columns are not zero, otherwise the generate board button does nothing")]
        public int rows;
        public int columns;

        [BoardEditorHelpInfoBox("Data For The Classic Board")]
        public float displacementX = 0;
        public float displacmentY = 0;

        [BoardEditorHelpInfoBox("You must provide a gameobject to be used as")]
        public GameObject classicBoardSpaceDefault;
        public Enums.BoardNeighborType typeOfNeighbours;

        [BoardEditorHelpInfoBox("You must provide a gameobject to be used as")]
        public GameObject offsetHexBoardSpaceDefault;


        public virtual void GenerateClassicBoard() {
            if (GetComponent<ClassicBoard>() != null) {
                DestroyImmediate(GetComponent<ClassicBoard>());
            }
            ClassicBoard testBoard = gameObject.AddComponent<ClassicBoard>();
            testBoard.classicBoardSpace = classicBoardSpaceDefault;
            testBoard.displacementInX = displacementX;
            testBoard.displacementInY = displacmentY;
            ClassicBoardData newData = new ClassicBoardData(columns, rows, typeOfNeighbours);
            testBoard.SetData(newData);
            testBoard.SetupBoard();
        }

        public virtual void GenerateOffsetHexagonalBoard()
        {
            if (GetComponent<ClassicBoard>() != null) {
                DestroyImmediate(GetComponent<OffsetHexBoard>());
            }
            OffsetHexBoard testBoard = gameObject.AddComponent<OffsetHexBoard>();
            testBoard.hexBoardSpace = offsetHexBoardSpaceDefault;
            OffsetHexBoardData newData = new OffsetHexBoardData(columns, rows, HexTypeEnum.ODD_Q_VERTICAL_LAYOUT);
            testBoard.SetData(newData);
            testBoard.SetupBoard();
        }

    }
}