﻿using BoardGameFramework.Core;
using BoardGameFramework.Enums;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BoardGameFramework.ClassicBoards
{
    [Serializable]
    public class ClassicBoardData : BoardData
    {
        [SerializeField]
        public BoardNeighborType typeOfNeighbors;
     

        public ClassicBoardData(int newRows, int newColumns) : base(newRows, newColumns)
        {
        }

        public ClassicBoardData(int newRows, int newColumns, BoardNeighborType boardType) : base(newRows, newColumns)
        {          
            typeOfNeighbors = boardType;
        }
    }
}