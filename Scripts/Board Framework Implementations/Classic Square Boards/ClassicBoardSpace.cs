﻿using BoardGameFramework.Core;
using System.Collections;
using System.Collections.Generic;
#if UNITY_EDITOR
using UnityEditor;
#endif
using UnityEngine;

namespace BoardGameFramework.ClassicBoards
{
    public class ClassicBoardSpace : BoardSpace<ClassicBoardSpaceData>
    {
        #if UNITY_EDITOR

        void OnDrawGizmos()
        {
            GUIStyle style = new GUIStyle();

            if (transform != null && data != null) {
                Handles.Label(transform.position, data.Row + "," + data.Column, style);

            }
        }

#endif
    }
}