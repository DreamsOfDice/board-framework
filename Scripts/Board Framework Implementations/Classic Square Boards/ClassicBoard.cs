﻿using BoardGameFramework.Core;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BoardGameFramework.ClassicBoards
{
    public class ClassicBoard : Board<ClassicBoardData>
    {
        public GameObject classicBoardSpace;
        public float displacementInY= 0;
        public float displacementInX = 0;

        protected void Awake()
        {
            if (data != null)
            {
                data.boardSpaces = new BoardSpaceBase[data.columns, data.rows];

                foreach (var space in GetComponentsInChildren<BoardSpaceBase>())
                {
                    var spaceData = space.GetData<BoardSpaceData>();
                    data.boardSpaces[spaceData.Column, spaceData.Row] = space;
                }

            }

        }

        protected override void CalculateNeighbours()
        {
            if (data.typeOfNeighbors == Enums.BoardNeighborType.FOUR_WAY_SQUARE_NEIGHBOR) {
                SetupFourWayNeighbours();
                return;
            }
            if (data.typeOfNeighbors == Enums.BoardNeighborType.EIGHT_WAY_SQUARE_NEIGHBOR)
            {
                SetupEightWayNeighbours();
                return;
            }
        }

        public override void SetupBoard()
        {
            List<BoardSpace<BoardSpaceData>> spaces = new List<BoardSpace<BoardSpaceData>>();
            GameObject boardHolder;
            if (transform.childCount == 0)
            {
                boardHolder = new GameObject("Board Holder");
                boardHolder.transform.SetParent(transform);

            }
            else
            {
                boardHolder = transform.GetChild(0).gameObject;
                DestroyImmediate(boardHolder);
                boardHolder = new GameObject("Board Holder");
                boardHolder.transform.SetParent(transform);

            }

            for (int i = 0; i < data.rows; i++)
            {
                for (int j = 0; j < data.columns; j++)
                {
                    //Vector2 sizeOfCellDefault = SpaceTemplate.GetComponent<SpriteRenderer>().size;
                    Vector2 sizeOfCellDefault = classicBoardSpace.GetComponent<SpriteRenderer>().bounds.size;

                    GameObject spaceInWorld = Instantiate(classicBoardSpace, new Vector3(0 + sizeOfCellDefault.x * j+displacementInX*j, 0 + (i * sizeOfCellDefault.y+displacementInY*i), 0), Quaternion.identity, boardHolder.transform);
                    spaceInWorld.GetComponent<SpriteRenderer>().sortingLayerName = "Grid";
                    spaceInWorld.GetComponent<SpriteRenderer>().sortingOrder = i*2;//done to leave a space for overlapping sprites
                    ClassicBoardSpaceData newSpaceData = new ClassicBoardSpaceData(i, j);
                    spaceInWorld.name = "Classic Space " +newSpaceData.Row+","+newSpaceData.Column;

                    ClassicBoardSpace newClassicSpace = spaceInWorld.GetComponent<ClassicBoardSpace>();
                    if (newClassicSpace == null) {
                        newClassicSpace = spaceInWorld.AddComponent<ClassicBoardSpace>();
                    }
                    newClassicSpace.data = newSpaceData;
                    //spaceInWorld.GetComponent<ClassicBoardSpace>().spaceControler.SetBoardUIReference(spaceInWorld.GetComponent<BoardSpaceUI>());
                    //  boardUISpaces.Add(spaceInWorld.GetComponent<BoardSpaceUI>());
                    data.boardSpaces[j, i] = newClassicSpace;

                }


            }

            /* //old part
             for (int x = 0; x < data.rows; x++)
             {
                 for (int y = 0; y < data.columns; y++)
                 {
                     data.boardSpaces[x, y] = spaces[x * data.columns + y].data;//remember that in a matrix x is left, so 0,1 0,2.... ect is x = 0 y = var
                 }
             }*/
            CalculateNeighbours();
        }

        protected virtual void SetupFourWayNeighbours() {
            for (int x = 0; x < data.columns; x++)
            {
                for (int y = 0; y < data.rows; y++)
                {
                    List<BoardSpaceBase> newNeighbours = new List<BoardSpaceBase>();

                    // This is the 4-way connection version:
                    if (x > 0)
                        newNeighbours.Add(data.boardSpaces[x - 1,y]);
                    if (x < data.columns - 1)
                        newNeighbours.Add(data.boardSpaces[x + 1,y]);
                    if (y > 0)
                        newNeighbours.Add(data.boardSpaces[x,y - 1]);
                    if (y < data.rows - 1)
                        newNeighbours.Add(data.boardSpaces[x,y + 1]);

                    data.boardSpaces[x, y].GetData<ClassicBoardSpaceData>().Neighbours = newNeighbours;
                        

                }

            }
        }

        protected virtual void SetupEightWayNeighbours()
        {
            
            for (int x = 0; x < data.columns; x++)
            {
                for (int y = 0; y < data.rows; y++)
                {

                    List<BoardSpaceBase> newNeighbours = new List<BoardSpaceBase>();


                    // This is the 8-way connection version (allows diagonal movement)
                    // Try down
                    if (x > 0)
                    {
                        newNeighbours.Add(data.boardSpaces[x - 1,y]);
                        if (y > 0)
                            newNeighbours.Add(data.boardSpaces[x - 1,y - 1]);
                        if (y < data.rows - 1)
                            newNeighbours.Add(data.boardSpaces[x - 1,y + 1]);
                    }

                    // Try up
                    if (x < data.columns - 1)
                    {
                        newNeighbours.Add(data.boardSpaces[x + 1,y]);
                        if (y > 0)
                            newNeighbours.Add(data.boardSpaces[x + 1,y - 1]);
                        if (y < data.rows - 1)
                            newNeighbours.Add(data.boardSpaces[x + 1,y + 1]);
                    }

                    // Try Left and Right
                    if (y > 0)
                        newNeighbours.Add(data.boardSpaces[x,y - 1]);
                    if (y < data.rows - 1)
                        newNeighbours.Add(data.boardSpaces[x,y + 1]);

                    data.boardSpaces[x,y].GetData<ClassicBoardSpaceData>().Neighbours = newNeighbours;
                }
            

        }
    }
    }
}