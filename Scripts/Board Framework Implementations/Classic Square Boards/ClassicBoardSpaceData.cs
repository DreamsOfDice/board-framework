﻿using BoardGameFramework.Core;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BoardGameFramework.ClassicBoards
{
    [Serializable]
    public class ClassicBoardSpaceData : BoardSpaceData
    {
        public ClassicBoardSpaceData(int row, int column) : base(row, column)
        {

        }

    }
}
    