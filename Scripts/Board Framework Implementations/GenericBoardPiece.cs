﻿using System.Collections;
using System.Collections.Generic;
using BoardGameFramework.Core;
using UnityEngine;

namespace BoardGameFramework.GenericImplementations{
public class GenericBoardPiece : BoardPiece<BoardPieceData>
{
        // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
       if(data.currentPath.Count > 0){
           MovementHandling();
       } 

    }
}
}