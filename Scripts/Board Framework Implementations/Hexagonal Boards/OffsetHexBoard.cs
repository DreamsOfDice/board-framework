﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using BoardGameFramework.Core;

namespace BoardGameFramework.HexBoards { 

    public class OffsetHexBoard : Board<OffsetHexBoardData>
    {
        public GameObject hexBoardSpace;

        public override void SetupBoard()
        {
            List<BoardSpace<BoardSpaceData>> spaces = new List<BoardSpace<BoardSpaceData>>();
            GameObject boardHolder;
            if (transform.childCount == 0)
            {
                boardHolder = new GameObject("Board Holder");
                boardHolder.transform.SetParent(transform);

            }
            else
            {
                boardHolder = transform.GetChild(0).gameObject;
                DestroyImmediate(boardHolder);
                boardHolder = new GameObject();
                boardHolder.transform.SetParent(transform);

            }

            switch (data.orientation) {
                case HexTypeEnum.ODD_Q_VERTICAL_LAYOUT:
                    OffsetOddQVerticalLayoutSetup(boardHolder);
                    break;
            }

            CalculateNeighbours();
        }

        protected override void CalculateNeighbours()
        {
            switch (data.orientation)
            {
                case HexTypeEnum.ODD_Q_VERTICAL_LAYOUT:
                    OddQOffsetNeighbor();
                    break;
            }
        }

        protected virtual void OffsetOddQVerticalLayoutSetup(GameObject boardHolder) {

            for (int i = 0; i < data.rows; i++)
            {
                for (int j = 0; j < data.columns; j++)
                {
                    //Vector2 sizeOfCellDefault = SpaceTemplate.GetComponent<SpriteRenderer>().size;
                    Vector2 sizeOfCellDefault = hexBoardSpace.GetComponent<SpriteRenderer>().bounds.size;
                    Debug.Log("The size of my sprite rendere is "+ sizeOfCellDefault);
                    int indentionValue = 0;
                    if (j%2 == 0 ) {
                        indentionValue = 1;
                    }
                    
                    GameObject spaceInWorld = Instantiate(hexBoardSpace, new Vector3((sizeOfCellDefault.x * (3f/4f)*j), (-i * sizeOfCellDefault.y)+(1/2f*indentionValue), 0), Quaternion.identity, boardHolder.transform);
                    //spaceInWorld.AddComponent<SpriteRenderer>().sprite = SpaceTemplate.sprite;
                    OffsetHexBoardSpaceData newSpaceData = new OffsetHexBoardSpaceData(i, j);


                    OffsetHexBoardSpace newHexSpace = spaceInWorld.AddComponent<OffsetHexBoardSpace>();
                    newHexSpace.data = newSpaceData;
                    spaceInWorld.name = "Hex Space " +newSpaceData.Row+","+newSpaceData.Column;
                    //spaceInWorld.GetComponent<ClassicBoardSpace>().spaceControler.SetBoardUIReference(spaceInWorld.GetComponent<BoardSpaceUI>());
                    //  boardUISpaces.Add(spaceInWorld.GetComponent<BoardSpaceUI>());
                    data.boardSpaces[j, i] = newHexSpace;

                }


            }

        }

        protected virtual void OddQOffsetNeighbor() {
            for (int x = 0; x < data.columns; x++)
            {
                for (int y = 0; y < data.rows; y++)
                {

                    List<BoardSpaceBase> newNeighbours = new List<BoardSpaceBase>();

                    if (y % 2 == 0){
                        if (y > 0)
                        {
                            newNeighbours.Add(data.boardSpaces[x , y-1]);

                            if (x > 0)
                                newNeighbours.Add(data.boardSpaces[x - 1, y - 1]);
                          
                        }

                        // Try right
                        if (y < data.rows - 1)
                        {
                            newNeighbours.Add(data.boardSpaces[x, y+1]);
                            if (x > 0)
                                newNeighbours.Add(data.boardSpaces[x - 1, y + 1]);
                          
                        }

                        // Try Left and Right
                        if (x > 0)
                            newNeighbours.Add(data.boardSpaces[x-1, y ]);

                        if (x < data.columns - 1)
                            newNeighbours.Add(data.boardSpaces[x+1, y]);

                    }
                    else {

                        if (y > 0)
                        {

                            newNeighbours.Add(data.boardSpaces[x , y-1]);

                            if (x < data.columns - 1)
                                newNeighbours.Add(data.boardSpaces[x + 1, y - 1]);

                        }

                        // Try right
                        if (y < data.rows - 1)
                        {
                            newNeighbours.Add(data.boardSpaces[x , y+1]);
                            if (x < data.columns - 1)
                                newNeighbours.Add(data.boardSpaces[x + 1, y + 1]);

                        }

                        // Try Left and Right
                        if (x > 0)
                            newNeighbours.Add(data.boardSpaces[x-1, y ]);

                        if (x < data.columns - 1)
                            newNeighbours.Add(data.boardSpaces[x+1, y]);

                    }
                                          
                    data.boardSpaces[x, y].GetData<OffsetHexBoardSpaceData>().Neighbours = newNeighbours;
                }


            }
        }

    }
}