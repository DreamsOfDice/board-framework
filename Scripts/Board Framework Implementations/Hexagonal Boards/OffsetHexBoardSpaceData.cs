﻿using BoardGameFramework.Core;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BoardGameFramework.HexBoards
{
    [Serializable]
    public class OffsetHexBoardSpaceData : BoardSpaceData
    {
        public OffsetHexBoardSpaceData(int row, int column) : base(row, column)
        {

        }
    }
}