﻿using BoardGameFramework.Core;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif
namespace BoardGameFramework.HexBoards
{
    public class OffsetHexBoardSpace : BoardSpace<OffsetHexBoardSpaceData>
    {
#if UNITY_EDITOR

        void OnDrawGizmos()
        {
            GUIStyle style = new GUIStyle();

            if (transform != null && data != null) {
                Handles.Label(transform.position, data.Row + "," + data.Column, style);

            }
        }

#endif

    }
}