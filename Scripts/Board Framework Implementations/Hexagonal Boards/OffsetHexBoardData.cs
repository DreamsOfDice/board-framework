﻿using BoardGameFramework.Core;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BoardGameFramework.HexBoards
{
    public class OffsetHexBoardData : BoardData
    {
        [SerializeField]
        public HexTypeEnum orientation;

        public OffsetHexBoardData(int newRows, int newColumns) : base(newRows, newColumns)
        {
        }

        public OffsetHexBoardData(int newRows, int newColumns, HexTypeEnum newOrientation) : base(newRows, newColumns)
        {
            orientation = newOrientation;
        }

    }
}